$(document).ready(function(){

  //---------------------------------Honey Trap---------------//

  $('.inputGhost').on('change', function() {
    $("input[type=submit]").prop('disabled', $(this).val().length > 0);


  //-----------------------------Fadeout Flash Notice-----------------//
  setTimeout(function() {
    $('#flash').fadeOut('slow');}, 3000
  );
});

  //-----------------------------Skills Hover-----------------------//

$('.skillsDouble li').hover(function () {
   $(this).find('.text').stop().hide();
   $(this).find('.star').stop().show();
 

}, function() {
   $(this).find('.star').stop().hide();
   $(this).find('.text').stop().show();
});
//-------------------------------------Twitter Feed---------------------------------------//

var $container = $('#innerTwitter');

    $.get('/feed', function(data) { showTweets($(data).slice(0,5)) }, 'json');

   function showTweets(tweets) {
    var tweetPs = $.map(tweets, function (t) {
         return $('<p></p>').text(t.text).hide();
    });

    $.each(tweetPs, function(_,ele) {
        $container.append(ele);
    });

    tweetPs[0].show();

    var currentIndex = 0;

    var nextTweet = function () {
        var nextIndex = currentIndex == tweetPs.length - 1 ? 0 : currentIndex + 1;
        tweetPs[currentIndex].fadeOut(400, function () {
            tweetPs[nextIndex].fadeIn(400);
        });
        currentIndex = nextIndex;
    };

    setInterval(nextTweet, 7000);
}

//--------------------------------------Slide Show--------------------------------------//
 

 $.vegas('slideshow', {
                backgrounds:[
                  { src:'/assets/images/mb.jpg', fade:1500 },
                  { src:'/assets/images/rugby.jpg', fade:1500 },
                 { src:'/assets/images/newyork.jpg', fade:1500 }
                ],
                loading:false
              })('overlay', {
                src:'/assets/images/02.png',
                opacity:'0.6'
              });


//------------------------------------- Navigation setup ------------------------------------------------//
  

//--------- Scroll navigation ---------------//
$("#mainNav a,.innerctLink").click(function(event){

  event.preventDefault();
  var full_url = this.href;
  var parts = full_url.split("#");
  var trgt = parts[1];
  var target_offset = $("#"+trgt).offset();
  var target_top = target_offset.top;

  $('html,body').animate({scrollTop:target_top -68}, 800);
    
  
});

//-------------Highlight the current section in the navigation bar------------//
var sections = $("section");
  var navigation_links = $("#mainNav a");
  
  sections.waypoint({
    handler: function(event, direction) {
    
      var active_section;
      active_section = $(this);
      if (direction === "up") active_section = active_section.prev();

      var active_link = $('#mainNav a[href="#' + active_section.attr("id") + '"]');
      navigation_links.removeClass("active");
      active_link.addClass("active");

    },
    offset: '35%'
  })


//-------Show / hide the new srtyled navigation depending of the position of the sections--------//
var menu = $('#mainNav'),
  pos = menu.offset();

  $(window).scroll(function(){
    if($(this).scrollTop() > pos.top+20 && menu.hasClass('default')){
      menu.fadeOut('fast', function(){
        $(this).removeClass('default').addClass('fixed').slideDown(200);
      });
    } else if($(this).scrollTop() <= pos.top+20 && menu.hasClass('fixed')){
      menu.slideUp(200, function(){
        $(this).removeClass('fixed').addClass('default').slideDown(200);
      });
    }
  });
  
  
  
    
//------------------------------------- End navigation setup ------------------------------------------------//


//--------------------------------- Hover animation for the elements of the portfolio --------------------------------//
        
        
        $("div.link").css({ opacity: 0 });
        $('.work, .item').hover( function(){ 
          $(this).children('img').animate({ opacity: 0.80 }, 'fast');
          $(this).children('.link').animate({ opacity: 0.95 }, 'fast');
        }, function(){ 
          $(this).children('img').animate({ opacity: 1 }, 'slow');
          $(this).children('.link').animate({ opacity: 0 }, 'slow'); 
        }); 
        
      

//--------------------------------- End hover animation for the elements of the portfolio --------------------------------//

//-----------------------------------Initilaizing fancybox for the portfolio-------------------------------------------------//

  $('.portfolio a.folio').fancybox({
          'titlePosition' : 'inside',
          'overlayShow' : true,
          'opacity'   : true,
          'transitionIn'  : 'elastic',
          'transitionOut' : 'none',
          'overlayOpacity'  :   0.8
        });
        
//-----------------------------------End initilaizing fancybox for the portfolio-------------------------------------------------//

  //--------------------------------- Sorting portfolio elements with quicksand plugin  --------------------------------//
  
    var $portfolioClone = $('.portfolio').clone();

    $('.filter a').click(function(e){
      $('.filter li').removeClass('current'); 
      var $filterClass = $(this).parent().attr('class');
      if ( $filterClass == 'all' ) {
        var $filteredPortfolio = $portfolioClone.find('li');
      } else {
        var $filteredPortfolio = $portfolioClone.find('li[data-type~=' + $filterClass + ']');
      }
      $('.portfolio').quicksand( $filteredPortfolio, { 
        duration: 800,
        easing: 'easeInOutQuad' 
      }, function(){
          $('.item').hover( function(){ 
            $(this).children('img').animate({ opacity: 0.80 }, 'fast');
            $(this).children('.link').animate({ opacity: 0.95 }, 'fast');
          }, function(){ 
            $(this).children('img').animate({ opacity: 1 }, 'slow');
            $(this).children('.link').animate({ opacity: 0 }, 'slow');
          }); 


//------------------------------ Reinitilaizing fancybox for the new cloned elements of the portfolio----------------------------//

        $('.portfolio a.folio').fancybox({
                'overlayShow' : true,
                'opacity'   : true,
                'transitionIn'  : 'elastic',
                'transitionOut' : 'none',
                'overlayOpacity'  :   0.8
              });

//-------------------------- End reinitilaizing fancybox for the new cloned elements of the portfolio ----------------------------//

      });


      $(this).parent().addClass('current');
      e.preventDefault();
    });

//--------------------------------- End sorting portfolio elements with quicksand plugin--------------------------------//


//--------------------------------- Form validation --------------------------------//
//---------------------------------- Forms validation -----------------------------------------//
  





//---------------------------------- Testimonials-----------------------------------------//
$('#testimonials').slides({
  preload: false,
  generateNextPrev: false,
  play: 4500,
  container: 'testimoniaContainer'
});
//---------------------------------- End testimonials-----------------------------------------//


    
});




