require 'bundler/setup'
Bundler.require(:default)
require 'rubygems'
require 'sinatra/base'
require 'sinatra/flash'
require 'pony'
require './config/config.rb' if File.exists?('./config/config.rb')
require 'sinatra/jsonp'
require 'twitter'
require 'sinatra/static_assets'
require './config/environments/development'
require './config/environments/production'
require './helpers/aws_helper.rb'



  
enable :sessions
enable :json_pretty

twitter_client = Twitter::Client.new(
    :consumer_key       => ENV["CONSUMER_KEY"],
    :consumer_secret    => ENV["CONSUMER_SECRET"],
    :oauth_token        => ENV["OAUTH_TOKEN"],
    :oauth_token_secret => ENV["OAUTH_SECRET"],
  )


get '/' do
  erb :index
end

post '/' do
    from = params[:name]
    subject = "#{params[:name]} has contacted you from your Personal Website"
    body = erb(:mail, layout: false)

  Pony.mail(
  :from => from,
  :to => ENV["EMAIL_ADDRESS"],
  :subject => subject,
  :body => body,
  :via => :smtp,
  :via_options => {
    :address              => 'smtp.gmail.com',
    :port                 => '587',
    :enable_starttls_auto => true,
    :user_name            => ENV["USER_NAME"],
    :password             => ENV["PASSWORD"],
    :authentication       => :plain, 
    :domain               => "localhost.localdomain" 
})
  flash[:notice] = "Thanks for your email. I will be in touch soon."
  redirect '/success' 


end

get '/success' do
  erb :index
end

get '/feed' do
   jsonp twitter_client.user_timeline('richl14').map(&:attrs)
end




